<!DOCTYPE html>

<html lang="fr">

<head>
    <meta charset="utf-8" />
    <title>RIP, le compteur.</title>
    <meta property="og:title" content="RIP, le compteur." />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://dav.li/rip-le-compteur/" />
    <meta property="og:description" content="Compteur collaboratif des signatures pour le référendum d'initiative partagée. #ReferendumADP" />
    <meta property="og:locale" content="fr_FR" />
    <link rel="shortcut icon" type="image/png" href="Logo.png"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#0070C0">
    <link rel="stylesheet" href="https://dav.li/forkawesome/1.0.11/css/fork-awesome.min.css" />
    <link rel="stylesheet" href="//dav.li/jquery/ui/jquery-ui.min.css" />
    <style>
        body {
            font-family: sans-serif;
            margin: 0;
        }

        header {
            font-size: 1.2em;
        }

        header,
        footer {
            padding: 20px 0;
            background-color: #3c3c3c;
            color: white;
            text-align: center;
        }

        header>*,
        footer>* {
            margin: 0 auto !important;
        }

        h1,
        h2,
        p,
        #tabs {
            margin: 20px auto;
            width: 80%;
        }

        h1 {
            font-size: 5em;
        }

        section {
            padding: 20px;
        }

        section:nth-child(odd) {
            background-color: #fdfdfd;
        }

        section:nth-child(even) {
            background-color: #f0f0f0;
        }

        section.centered {
            text-align: center;
        }

        footer {
            position: fixed;
            bottom: 0;
            right: 0;
            left: 0;
        }

        a {
            color: inherit;
            font-weight: bold;
            text-decoration: none;
        }

        a:hover {
            text-decoration: underline;
        }

        .btn {
            background-color: #f0f0f0;
            border-radius: 5px;
            padding: 10px;
            border: 2px solid #f0f0f0;
        }

        .btn:hover {
            text-decoration: none;
            border-color: #3c3c3c;
        }

        .btn:active {
            opacity: 0.5;
        }

        .stats {
            display: block;
            margin: auto;
            overflow: auto;
        }

        .stats th {
            text-align: left;
        }

        .stats .bar {
            display: inline-block;
            background-color: #9d9d9d;
        }

        #progressbar{
            background-color: #f0f0f0;
            width: 80%;
            margin: auto;
            margin-top: 35px;
        }
        #progressbar>span{
            display: block;
            position: relative;
            height: 10px;
            background-color: #13bc3a;
        }
        #progressbar>span>span{
            position: absolute;
            top: -33px;
            right: -27px;
            padding: 5px;
            background-color: #f0f0f0;
            border-radius: 5px;
        }
        #progressbar>span>span::before{
            content: "";
            position: absolute;
            top: 100%;
            left: 22px;
            width: 0;
            height: 0;
            border-left: 6px solid transparent;
            border-top: 6px solid #f0f0f0;
            border-right: 6px solid transparent;
        }
    </style>
</head>

<body>
    <header>
        <h3>RIP, le compteur.</h3>
    </header>
    <main>
        <section class="centered">
            <?php
        $alphabet   = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $bdd        = [];
        ini_set('auto_detect_line_endings',TRUE);
        $handle = fopen('data.txt', 'r');
        while ( ($data = fgetcsv($handle, 1000)) !== FALSE ) {
            if ($data[0] === NULL) {
                continue;
            }

            $section = $data[3];
            if (!isset($bdd[$section])) {
                $bdd[$section] = [];
            }
            $bdd[$section][] = [
                'date'      => $data[0],
                'section'   => $section,
                'compteur'  => intval($data[4] ?? -1),
                'nb_pages'  => intval($data[5] ?? -1),
                'is_last'   => boolval($data[6] ?? FALSE),
            ];
        }
        ini_set('auto_detect_line_endings', FALSE);
        $total          = 0;
        $withPagination = 0;

        // Pour avoir la bdd avec la dernière donnée seulement
        $bdd_last   = [];
        // Parmi la liste de données par section, on prend la "meilleure" (selon les paramètres qu'on veut)
        $bdd_best   = [];
        foreach ($bdd as $section => $lignes) {
            $bdd_last[$section] = end($lignes);
            // On parcourt en partant de la fin
            for ($k = count($lignes) - 1; $k >= 0; $k--) {
                if ($lignes[$k]['is_last']) {
                    $bdd_best[$section] = $lignes[$k];
                    break;
                }
            }
            if (!isset($bdd_best[$section])) {
                // On a pas trouvé de compte exacte, on prend le dernier
                $bdd_best[$section] = $bdd_last[$section];
                $withPagination++;
            }
            $total += $bdd_best[$section]['compteur'];
        }

        $pourcentage = round($total/4693375*100, 2);

        ?>
                <h1>
                    <?php echo($total); ?>
                </h1>
                <p>Signatures détectées* sur la <a href="https://www.referendum.interieur.gouv.fr/" target="_blank">proposition de loi référendaire</a> visant à affirmer le caractère de service public national de l'exploitation des aérodromes de Paris sur 4 693 375 nécessaires, soit <?php echo($pourcentage); ?>%.
                    <div id="progressbar"><span style="width:<?php echo($pourcentage); ?>%"><span><?php echo($pourcentage); ?>%</span> </span></div>
                </p>
                <p><i>*Données collectées collaborativement avec une marge d'erreur de plus ou moins
                    <?php echo($withPagination*100); ?> signatures dû au mode de calcul (le chiffre exact se situe donc entre
                    <?php echo($total-$withPagination*100); ?> et
                    <?php echo($total+$withPagination*100); ?>). Des erreurs peuvent altérer la véracité de ce chiffre. Toutes les données sont <a href="data.txt" target="_blank">en libre accès</a> pour faire vos propres calculs et vérifications.</i></p>
        </section>
        <section>
            <h2>Aider à améliorer cette statistique en installant un module sur votre navigateur</h2>
            <p>Le site du ministère de l'intérieur ne permet pas d'obtenir le nombre de signatures total. Pour avoir ce chiffre, il faudrait que chacun compte les signatures affichées sur le site web. Heureusement, un script permet de le faire à votre place et de transmettre ce résultat à ce site web. L'objectif est de centraliser le comptage fait par les citoyens qui ont installé l'extension sur leur navigateur et ainsi d'avoir le nombre de signataires.</p>
            <p>Après avoir installé l'extension, il vous faudra simplement visiter des pages tel que https://www.referendum.interieur.gouv.fr/consultation_publique/8/A/AA et résoudre les "captcha". L'extension détectera automatiquement les signatures. Merci !</p>
        </section>
        <section class="centered">
            <p><a href="rip_le_compteur-1.3-fx.xpi" class="btn"><i class="fa fa-download" aria-hidden="true"></i> Installer l'extension sur Firefox</a></p>
            <p>Version 1.3 (mise à jour le 19/06/19 à 9h00)</p>
        </section>
        <section>
            <h2>Avertissement et conditions d'utilisation</h2>
            <p>L'auteur de ce site web et de l'extension ne peut être tenu responsable d'une mauvaise utilisation ou d'une utilisation malicieuse des outils ou données délivrés par ce site web.</p>
            <p>Les données de ce site web sont en libre accès (<a href="data.txt" target="_blank">base de données compteur</a>, <a href="error.txt" target="_blank">base de données erreur</a>, <a href="https://framagit.org/DavidLibeau/rip-le-compteur" target="_blank">code source</a>) et sont soumis à leur licence respective.</p>
            <p>Les utilisateurs de l'extension de navigateur sont contributeurs et publient les données qu'ils récoltent sous licence Creative Commons Attribution.</p>
            <p>L'auteur de ce site web et de l'extension ne fournit aucune garantie quant à la validité des données fournies par ce site web.</p>
            <p>En utilisant ce site web et en téléchargement l'extension pour navigateur, les utilisateurs donnent leur approbation concernant ces conditions d'utilsation et leur consentement à l'utilisation et la transmission des données necessaire au bon fonctionnement de ce site web et de l'extension pour navigateur.</p>
            <p>Ce site web et l'extension de navigateur peuvent enregistrer les données de connexion de l'utilisateur et notament son adresse IP. Ce site web utilise des cookies necessaire à son fonctionnement. Aucune donnée n'est vendue. Certaines données sont en libre accès (<a href="data.txt" target="_blank">base de données compteur</a>, <a href="error.txt" target="_blank">base de données erreur</a>). L'adresse IP de l'utilisateur est anonymisée (pseudonymisation avec un algorithme sha256 avec ajout d'une chaine de caractères secrete) avant d'être sauvegardée.</p>
            <p>Avant d'utiliser l'extension de navigateur, l'utilisateur doit s'informer de son fonctionnement en consultant le code source en libre accès (<a href="https://framagit.org/DavidLibeau/rip-le-compteur" target="_blank">à cette adresse</a>) ou en demandant des informations à l'auteur de l'extension, si l'utilisateur a des questions.</p>
            <p>L'utilisateur peut à tout moment exercer ses droits auprès de l'auteur du site web et de l'extension de navigateur. L'auteur du site web et de l'extension de navigateur aura 30 jours pour répondre et exercer les droits de l'utilisateur.</p>
            <p>L'auteur de ce site web et de l'extension de navigateur peut être contacté <a href="https://davidlibeau.fr/Contact" target="_blank">à cette adresse</a>.</p>
        </section>
        <section>
            <h2>Statistiques supplémentaires</h2>
            <div id="tabs">
                <ul>
                    <li><a href="#tabs-1">Alphabétique</a></li>
                    <li><a href="#tabs-2">Popularité</a></li>
                    <li><a href="#tabs-3">A mettre à jour</a></li>
                    <li><a href="#tabs-4">A préciser</a></li>
                </ul>
                <div id="tabs-1">
                    <table class="stats">
                        <?php
                        foreach ($bdd_best as $s => $donnees) {
                            echo('<tr>');
                            echo('<th><a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$s[0].'/'.$s.'" target="_blank">'.$s.'</a></th>');
                            echo('<th class="bar" style="width:'.($donnees['compteur']/10).'px">'.$donnees['compteur'].'</th>');
                            echo('</tr>'."\n");
                        }
                    ?>
                    </table>
                </div>
                <div id="tabs-2">
                    <table class="stats">
                        <?php
                        $bdd_sorted = $bdd_best;
                        uasort($bdd_sorted, function($a, $b) {
                            return $b['compteur'] <=> $a['compteur'];
                        });
                        foreach ($bdd_sorted as $s => $donnees) {
                            echo('<tr>');
                            echo('<th><a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$s[0].'/'.$s.'" target="_blank">'.$s.'</a></th>');
                            echo('<th class="bar" style="width:'.($donnees['compteur']/10).'px">'.$donnees['compteur'].'</th>');
                            echo('</tr>'."\n");
                        }
                        ?>
                    </table>
                </div>
                <div id="tabs-3">
                    <table class="stats">
                        <?php
                        $bdd_sorted = $bdd_last;
                        uasort($bdd_sorted, function($a, $b) {
                            return DateTime::createFromFormat('d/m/y H:i:s', $a['date']) <=> DateTime::createFromFormat('d/m/y H:i:s', $b['date']);
                        });
                        foreach ($bdd_sorted as $s => $donnees) {
                            echo('<tr>');
                            echo('<th><a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$s[0].'/'.$s.'" target="_blank">'.$s.'</a></th>');
                            echo('<th><a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$s[0].'/'.$s.'" target="_blank">'.$donnees['date'].'</a></th>');
                            echo('</tr>'."\n");
                        }
                        ?>
                    </table>
                </div>
                <div id="tabs-4">
                    <table class="stats">
                        <?php
                        $bdd_sorted = $bdd_best;
                        uasort($bdd_sorted, function($a, $b) {
                            return DateTime::createFromFormat('d/m/y H:i:s', $a['date']) <=> DateTime::createFromFormat('d/m/y H:i:s', $b['date']);
                        });
                        foreach ($bdd_sorted as $s => $donnees) {
                            if (($donnees['compteur'] > 200) && !$donnees['is_last']) {
                                echo('<tr>');
                                echo('<th><a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$s[0].'/'.$s.'" target="_blank">'.$s.'</a></th>');
                                echo('<th><a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$s[0].'/'.$s.'" target="_blank">'.$donnees['date'].' n\'est pas précis</a></th>');
                                echo('</tr>'."\n");
                            }
                        }
                    ?>
                    </table>
                </div>
            </div>
        </section>
    </main>
    <footer>
        Données CC-BY David Libeau & contributeur·rice·s / Code GNU AGPLv3 (<a href="https://framagit.org/DavidLibeau/rip-le-compteur" target="_blank">open source</a>)
    </footer>
    <script src="//dav.li/jquery/3.1.1.min.js"></script>
    <script src="//dav.li/jquery/ui/jquery-ui.min.js"></script>
    <script>
        $(function() {
            $("#tabs").tabs();
            $(window).scroll(function(evt) {
                if ($(window).scrollTop() < $(document).height() / 2) {
                    $("footer").css("bottom", "-" + $(window).scrollTop() + "px");
                } else {
                    $("footer").css("bottom", "-" + ($(document).height() - ($(window).scrollTop() + $(window).height())) + "px");
                }
            });
        });
    </script>
</body>

</html>
